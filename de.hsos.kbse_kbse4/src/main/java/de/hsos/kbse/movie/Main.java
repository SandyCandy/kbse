package de.hsos.kbse.movie;

import de.hsos.kbse.movie.impl.MovieLister;
import de.hsos.kbse.movie.impl.Movie;
import java.util.List;
import java.util.Scanner;
import org.jboss.weld.environment.se.Weld;


/**
 *
 * @author SandyCandy
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Find the movies!");
        
        String director;
  
        MovieLister getMovies = new Weld().initialize().instance().select( MovieLister.class ).get();
        
        
        while(true)
        {
            String newSearch;
            System.out.println("Enter a Name:");
            director = scanner.nextLine();
            
            List<Movie> myMovies = getMovies.moviesDirectedBy(director);
            System.out.println("Searching for movies by " + director + "...");

            System.out.println("\n" + myMovies.size() + " movies found!");

            for(Movie t : myMovies)
            {
                System.out.println(t.getTitle());
            }
            
            System.out.println("\nNew search? (Press 'y')\nStop searching? (Press any other key)\n");

            newSearch = scanner.nextLine();
            if(!newSearch.equalsIgnoreCase("y"))
            {
                break;
            }
            
        }
        
        System.out.println("Bye, bye!");
        

    }
    
}
