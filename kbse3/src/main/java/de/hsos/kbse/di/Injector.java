package de.hsos.kbse.di;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Injector {
    
    private static Injector instance = null;

    private Injector() {
    }
    
    public static Injector getInstance() {
        if(instance == null) {
            instance = new Injector();
        }
        return instance;
    }
    
    /* Erzeugt mit dem übergebenen String ein Class-Object. 
       Per Reflection werden sämtliche Fields abgefragt (auch privat!)
       Für Fields prüfen, ob mit Injection annotiert
    */
    public void inject(String classNameWithInjectionPoint) {
        try {
            Class<?> c = Class.forName(classNameWithInjectionPoint);
            Constructor<?> constr = c.getDeclaredConstructor();
            constr.setAccessible(true);
            Object obj = constr.newInstance();
            
            for (Field field : c.getDeclaredFields()) {
                String fieldName = field.getName();
                System.out.println("Fieldname = " + fieldName);
                if(field.isAnnotationPresent(Inject.class)) 
                {
                    field.setAccessible(true);
                    Class<?> fieldType = field.getType();
                    if(fieldType.isInterface()) 
                    {
                        Inject annotationClass = field.getAnnotation(Inject.class);
                        String className = annotationClass.instanceName();
                        
                        //Klasse instanziieren
                        Class<?> classInAnnotation = Class.forName(className);
                        Constructor<?> constructor = classInAnnotation.getConstructor();
                        Object o = constructor.newInstance();
                        field.set(obj, o);
                    } 
                    else 
                    {
                       // kein Interface -> Datentyp einfach instanziieren.
                       Constructor<?> constructor = fieldType.getConstructor();
                       Object o = constructor.newInstance();
                       field.set(obj, o);
                    }                 
                }
            }
        } 
        catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Injector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
