/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.moviefinder.db;

import de.hsos.kbse.moviefinder_if.MovieDTO;
import de.hsos.kbse.moviefinder_if.MovieFinder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DBMovieFinder implements MovieFinder {
    
    private String conName;

    /*Diese Methode liest eine Textdatei mit Filminformationen zeilenweise ein, erzeugt pro Zeile ein Objekt der Klasse
    Movie, verwaltet die erzeugten Objekte in einer Liste des Typs ArrayList<Movie> und gibt diese zurück*/
    public DBMovieFinder() {     
        conName = "jdbc:derby://localhost:1527/KBSE_2_JDBC";
    }

    public void setConnection(String connection) {
        this.conName = connection;
    }
       
    @Override
    public List<MovieDTO> findAllMovies() {
        List<MovieDTO> movies = new ArrayList<>();
   
        try {
         
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            try (Connection con = DriverManager.getConnection(conName))
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM movies");
                while(rs.next())
                {
                    //System.out.println(rs.getInt(1) + " " + rs.getString(2) + ": " + rs.getString(3));
                    MovieDTO m = new MovieDTO();
                    m.setDirector(rs.getString(3));
                    m.setTitle(rs.getString(2));
                    
                    movies.add(m);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }   catch (ClassNotFoundException ex) {
            Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return movies;
    }
    
    @Override
    public List<MovieDTO> findMoviesByDirector(String director) {
        
        if(director == null)
        {
            return null;
        }
        
        List<MovieDTO> movies = new ArrayList<>();
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            try (Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/KBSE_2_JDBC"))
            {
                PreparedStatement pst = con.prepareStatement("Select * from movies where dname like ?");
                pst.setString(1, "%" + director +" %");
                ResultSet rs = pst.executeQuery();
                while(rs.next())
                {
                    MovieDTO m = new MovieDTO();
                    m.setDirector(rs.getString(3));
                    m.setTitle(rs.getString(2));

                    movies.add(m);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            }

            return movies;
        }   catch (ClassNotFoundException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            }

        return movies;
    }




   
}
