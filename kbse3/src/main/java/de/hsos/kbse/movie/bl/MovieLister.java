package de.hsos.kbse.movie.bl;

//import de.hsos.kbse.di.Inject;
import de.hsos.kbse.di.Inject;
import de.hsos.kbse.movie_if.GetMovies;
import de.hsos.kbse.moviefinder_if.MovieFinder;
import de.hsos.kbse.moviefinder_if.MovieDTO;
import de.hsos.kbse.movie.dal.Movie;
import java.util.HashSet;
import java.util.Set;


public class MovieLister implements GetMovies
{
    private static MovieLister instance;
    private Set<Movie> movies = null;

    @Inject(instanceName = "de.hsos.kbse.moviefinder.csv.CSVMovieFinder")
    private MovieFinder movieFinder;
    
    private MovieLister() {
        if (instance == null) {
            instance = this;
        }
    }
    
    public static MovieLister getInstance(){
        if (MovieLister.instance == null) {
            MovieLister.instance = new MovieLister();
        }
        return MovieLister.instance;
    }

    public void createMovie(MovieDTO movie) {
        Movie m = new Movie(movie.getDirector(), movie.getTitle());
        movies.add(m);
    }
    
    public void loadAllMoviesFromMovieFinder() {
        this.movieFinder.findAllMovies().stream().forEach(m -> createMovie(m));        
    }

    @Override
    public Movie[] moviesDirectedBy(String directorName) {
        if(movies == null) {    
            movies = new HashSet<>();
            loadAllMoviesFromMovieFinder();
        }
        return this.movies
                .stream()
                .filter(m -> m.getDirector().contains(directorName))
                .toArray(Movie[]::new);        
    }   
}
