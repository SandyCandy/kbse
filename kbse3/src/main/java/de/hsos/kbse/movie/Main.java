package de.hsos.kbse.movie;

import de.hsos.kbse.di.Injector;
import de.hsos.kbse.movie.bl.MovieLister;
import de.hsos.kbse.movie.dal.Movie;
import de.hsos.kbse.movie_if.GetMovies;
import de.hsos.kbse.moviefinder.db.DBMovieFinder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


/**
 *
 * @author SandyCandy
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Find the movies!");
        
        String director;
        

        Injector injector = Injector.getInstance();
        injector.inject("de.hsos.kbse.movie.bl.MovieLister");

        GetMovies getMovies = MovieLister.getInstance();
        
//        String newSearch;
//        System.out.println("Enter a Name:");
//        director = scanner.nextLine();
//
//        Movie[] myMovies = getMovies.
//                moviesDirectedBy(director);
//        System.out.println("Searching for movies by " + director + "...");
//
//        System.out.println("\n" + myMovies.length + " movies found!");
//
//        for(Movie t : myMovies)
//        {
//            System.out.println(t.getTitle());
//        }
        
        while(true)
        {
            String newSearch;
            System.out.println("Enter a Name:");
            director = scanner.nextLine();
            
            Movie[] myMovies = getMovies.moviesDirectedBy(director);
            System.out.println("Searching for movies by " + director + "...");

            System.out.println("\n" + myMovies.length + " movies found!");

            for(Movie t : myMovies)
            {
                System.out.println(t.getTitle());
            }
            
            System.out.println("\nNew search? (Press 'y')\nStop searching? (Press any other key)\n");

            newSearch = scanner.nextLine();
            if(!newSearch.equalsIgnoreCase("y"))
            {
                break;
            }
            
        }
        
        System.out.println("Bye, bye!");
        

    }
    
}
