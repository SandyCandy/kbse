/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movie_if;

import de.hsos.kbse.movie.dal.Movie;

/**
 *
 * @author SandyCandy
 */
public interface GetMovies {
    public Movie[] moviesDirectedBy(String directorName);
}
