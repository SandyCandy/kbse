package de.hsos.kbse.boundary;

import de.hsos.kbse.dal.UserRepository;
import de.hsos.kbse.models.User;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
@Named("createlogin")
public class CreateLogin {
    
    @Inject
    private UserRepository userRepo;
    
    private String pwagain = "";

    public String getPwagain() {
        return pwagain;
    }

    public void setPwagain(String pwagain) {
        this.pwagain = pwagain;
    }
    
    
   @Inject
    private User newUser;
    
    public User getNewUser() {
        return newUser;
    }
    
    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }
   
    public void saveUser(User user) {
        userRepo.create(user);
    }
}
