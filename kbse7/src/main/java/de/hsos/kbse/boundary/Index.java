/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.boundary;

import de.hsos.kbse.dal.UserRepository;
import de.hsos.kbse.models.User;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Index {
   
    @Inject
    private UserRepository userRepo;
    
    private boolean showUserDetails = false;
    private boolean echoUserDetails = false;
    private User newUser = User.createNewUserObject();

    public boolean isShowUserDetails() {
        return showUserDetails;
    }

    public void setShowUserDetails(boolean showUserDetails) {
        this.showUserDetails = showUserDetails;
    }

    public boolean isEchoUserDetails() {
        return echoUserDetails;
    }

    public void setEchoUserDetails(boolean echoUserDetails) {
        this.echoUserDetails = echoUserDetails;
    }
    
    public User getNewUser() {
        return newUser;
    }
    
    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }
   
    public void saveUser(User user) {
        newUser = user;
        //userRepo.create(user);
    }
}
