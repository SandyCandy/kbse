/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.models;

import javax.enterprise.context.Dependent;

@Dependent
public class User {
    private String vorname = "";
    private String nachname = "";
    private Profil userProfil;
    private Login userLogin;

    public static User createNewUserObject() {
        User u = new User();
        u.setUserProfil(new Profil());
        u.setUserLogin(new Login());
        return u;
    }
    
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Profil getUserProfil() {
        return userProfil;
    }

    public void setUserProfil(Profil userProfil) {
        this.userProfil = userProfil;
    }

    public Login getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(Login userLogin) {
        this.userLogin = userLogin;
    }
    
}
