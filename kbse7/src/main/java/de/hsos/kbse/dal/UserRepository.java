/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.dal;

import de.hsos.kbse.models.User;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository {
    private ConcurrentHashMap<String, User> allUsers = new ConcurrentHashMap<>();
    
    public void create(User newUser) {
        allUsers.put(newUser.getUserLogin().getUsername(), newUser);
    } 
    
    // @Nullable
    public User read(String id) {
        if(!allUsers.contains(id)){
            return null;
        }
        return allUsers.get(id);
    }
    
    public void update(User newUser) {
        // yolo
        create(newUser);
    }
    
    public void delete(String id) {
        allUsers.remove(id);
    } 
}
