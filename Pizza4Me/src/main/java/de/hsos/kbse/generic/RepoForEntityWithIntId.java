package de.hsos.kbse.generic;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class RepoForEntityWithIntId<T extends EntityWithIntId> implements Serializable{
    @PersistenceContext(unitName="JPAPizzaPU")
    protected EntityManager em;
    protected Class<?> type;
    
    public void persist(T entity){
        em.persist(entity);
    }
    
    public T findById(int id){
        return (T)em.find(this.type, id);
    }
       
    public void remove (T entity){
        em.remove(entity);
    }
    public T merge(T entity){
        return em.merge(entity);
    }
    
}
