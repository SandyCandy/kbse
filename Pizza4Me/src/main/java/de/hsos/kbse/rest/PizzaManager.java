package de.hsos.kbse.rest;

import de.hsos.kbse.pizza4me.controller.BestellpostenRepo;
import de.hsos.kbse.pizza4me.controller.BestellungRepo;
import de.hsos.kbse.pizza4me.controller.KundeRepo;
import de.hsos.kbse.pizza4me.entity.Bestellposten;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Stateless
public class PizzaManager {
    
    @Inject
    private KundeRepo userRepo;
    
    @Inject
    private BestellungRepo orderRepo;
    
    @Inject
    private BestellpostenRepo postenRepo;
    
    @Context
    UriInfo uriInfo;
    
    public boolean saveOrder(Bestellung b){
        orderRepo.persist(b);
        return true;
        
    }
    
    public boolean updateOrder(Bestellung b){
        orderRepo.merge(b);
        return true;
    }
    
    public boolean updatePost(Bestellposten bp) {
        postenRepo.merge(bp);
        return true;
    }
    
    public boolean deletePost(Bestellung bp) {
        Bestellung bpTemp = orderRepo.merge(bp);
        orderRepo.remove(bpTemp);
        return true;
    }
}
