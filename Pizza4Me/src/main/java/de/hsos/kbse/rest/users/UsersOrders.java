/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.users;

import de.hsos.kbse.rest.exclusions.KundeExclusions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.pizza4me.entity.Kunde;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jonas
 */
@RequestScoped
@Path("users/{id}/orders")
@Produces(MediaType.APPLICATION_JSON)
public class UsersOrders {
    
    @Context
    UriInfo uriInfo;
   
    @Inject
    private List<Kunde> users;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllUsersOrders(@PathParam("id") int id) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new KundeExclusions())
                .serializeNulls()
                .create();
        
        Optional<Kunde> user = users.stream()
                .filter(u -> u.getId() == id)
                .findFirst();
        
        if(!user.isPresent()){
            return Response.noContent().build();
        }
        
        return Response.ok(gson.toJson(user.get().getBestellungen())).build();
    }
}
