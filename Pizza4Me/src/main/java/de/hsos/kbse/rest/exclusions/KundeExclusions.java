/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.exclusions;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 *
 * @author jonas
 */
public class KundeExclusions implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes fa) {
        return fa.getName().toLowerCase().equals("kunde");
    }

    @Override
    public boolean shouldSkipClass(Class<?> type) {
        return false;
    }
    
}
