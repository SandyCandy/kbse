/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.orders;

import de.hsos.kbse.pizza4me.controller.BestellungRepo;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.pizza4me.entity.Kunde;
import de.hsos.kbse.rest.PizzaManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jonas
 */
@RequestScoped
@Path("orders")
@Produces(MediaType.APPLICATION_JSON)
public class CreateOrder {
    
    @Inject
    private PizzaManager repo;
    
    @Inject
    private List<Kunde> users;
    
    @POST
    public Response createOrder(@QueryParam("email") String username) {
        Optional<Kunde> ok = users.stream()
                .filter(u -> u.getLogin().getEmail().equals(username))
                .findFirst();
        
        if(!ok.isPresent()){
            return Response.accepted("{}").build();
        }
        Bestellung b = new Bestellung();
        b.setKunde(ok.get());
        b.setPosten(new ArrayList<>());
        // hier muss eine transaction stattfinden
        repo.saveOrder(b);
        return Response.ok().build();
    }
    
    
}
