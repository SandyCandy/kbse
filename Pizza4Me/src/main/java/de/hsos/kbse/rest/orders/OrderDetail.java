/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.orders;

import de.hsos.kbse.rest.exclusions.KundeExclusions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hsos.kbse.pizza4me.controller.BestellungRepo;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.rest.PizzaManager;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jonas
 */
@RequestScoped
@Path("orders/{id}")
@Produces(MediaType.APPLICATION_JSON)
public class OrderDetail {
    @Context
    UriInfo uriInfo;
   
    @Inject
    private BestellungRepo repo;
    
    @Inject 
    PizzaManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response orderDetail(@PathParam("id") int id) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new KundeExclusions())
                .serializeNulls()
                .create();
        
        return Response.accepted(gson.toJson(
                repo.findById(id) != null ? repo.findById(id) : new Bestellung()
        )).build();
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDetail(@PathParam("id") int id) {
        
        manager.deletePost(repo.findById(id));
        return Response.ok("{ \"success\": true }").build();
    }
}
