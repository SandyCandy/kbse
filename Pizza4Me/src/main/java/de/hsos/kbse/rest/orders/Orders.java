/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.orders;

import de.hsos.kbse.rest.exclusions.KundeExclusions;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jonas
 */
@RequestScoped
@Path("orders")
@Produces(MediaType.APPLICATION_JSON)
public class Orders {
    @Context
    UriInfo uriInfo;
   
    @Inject
    private List<Bestellung> bestellungs;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllPizzas() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new KundeExclusions())
                .serializeNulls()
                .create();
        
        return Response.accepted(gson.toJson(bestellungs)).build();
    }
}
