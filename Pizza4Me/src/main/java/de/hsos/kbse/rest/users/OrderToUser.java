/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.users;

import de.hsos.kbse.rest.exclusions.KundeExclusions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hsos.kbse.pizza4me.controller.BestellungRepo;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.pizza4me.entity.Kunde;
import de.hsos.kbse.rest.PizzaManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jonas
 */
@RequestScoped
@Path("users/{id}/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderToUser {
    
    @Context
    UriInfo uriInfo;
   
    @Inject
    private List<Kunde> users;
    
    @Inject 
    private BestellungRepo repo;
    
    @Inject
    PizzaManager manager;
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response orderToUser(@PathParam("id") int id, @QueryParam("orderid") int orderId) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new KundeExclusions())
                .serializeNulls()
                .create();
        
        Bestellung b = repo.findById(orderId);
        if(b == null){
            return Response.noContent().build();
        }
        Optional<Kunde> k = users.stream()
                .filter(u -> u.getId() == id)
                .findFirst();
        if(!k.isPresent()){
            return Response.accepted("{}").build();
        }
        
        b.setKunde(k.get());
        manager.updateOrder(b);
        
        return Response.ok(gson.toJson(b)).build();
    }
}
