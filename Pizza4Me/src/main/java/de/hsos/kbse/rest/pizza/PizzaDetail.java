/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.pizza;

import com.google.gson.Gson;
import de.hsos.kbse.pizza4me.entity.Pizza;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jonas
 */
@RequestScoped
@Path("pizzas/{id}")
@Produces(MediaType.APPLICATION_JSON)
public class PizzaDetail {
    
    @Context
    UriInfo uriInfo;
   
    @Inject
    private List<Pizza> pizzas;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllPizzas(@PathParam("id") int id) {
        return Response.accepted(new Gson().toJson(
                pizzas.stream()
                        .filter(p -> p.getId() == id)
                        .findFirst()
                        .orElse(new Pizza())
        )).build();
    }
}
