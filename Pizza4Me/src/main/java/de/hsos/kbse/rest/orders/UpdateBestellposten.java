/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.rest.orders;

import de.hsos.kbse.pizza4me.controller.BestellpostenRepo;
import de.hsos.kbse.pizza4me.controller.BestellungRepo;
import de.hsos.kbse.pizza4me.entity.Bestellposten;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.rest.PizzaManager;
import java.util.Optional;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jonas
 */
@Path("/orders/{id}")
@Produces(MediaType.APPLICATION_JSON)
public class UpdateBestellposten {
    
    @Inject
    private PizzaManager repo;
    
    @Inject
    private BestellungRepo bestellrepo;
    
    @PUT
    public Response update(@PathParam("id") int id, @QueryParam("count") int count, @QueryParam("pizza") String pizzaname) {
        Bestellung b = bestellrepo.findById(id);
        if(b == null){
            return Response.accepted("bestellung null").build();
        }
        Optional <Bestellposten> bposten = b.getPosten().stream()
                .filter(p -> p.getPizza().getName().toLowerCase().equals(pizzaname.toLowerCase()))
                .findFirst();
        if(!bposten.isPresent()){
            return Response.accepted("bestellposten null").build();
        }
        
        bposten.get().setAnzahl(count);
        repo.updateOrder(b);
        return Response.ok().build();
    }
    
    @POST
    public Response create(@PathParam("id") int id, @QueryParam("count") int count, @QueryParam("pizza") String pizzaname) {
        return update(id, count, pizzaname);
    }
}
