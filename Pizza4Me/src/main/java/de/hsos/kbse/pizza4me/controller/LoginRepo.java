package de.hsos.kbse.pizza4me.controller;

import de.hsos.kbse.generic.RepoForEntityWithIntId;
import de.hsos.kbse.pizza4me.entity.Login;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TypedQuery;

@ApplicationScoped
public class LoginRepo extends RepoForEntityWithIntId<Login>{
    public LoginRepo(){
        this.type = Login.class;
    }
    
    public Login userLogin(String email){
        String queryString = "select l from Login l where l.email=:email";
        TypedQuery<Login> loginQuery = em.createQuery(queryString, Login.class);
        // Set the parameter
        loginQuery.setParameter("email", email);
        
        try{
            // Try and get one result
            Login temp = loginQuery.getSingleResult();
            return temp;
        } catch(Exception e){
            return null;
        }
    }
    
    public int amount(){
        TypedQuery<Login> loginQuery = em.createQuery("select l from Login l", Login.class);
        return loginQuery.getResultList().size();
    }
}

