package de.hsos.kbse.pizza4me.entity;

import de.hsos.kbse.generic.EntityWithIntId;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.OneToOne;

@Entity
@Access(AccessType.FIELD)
public class Login extends EntityWithIntId{
    private String email;
    private String password;
    
    private long unlocktime = 0;
    private int attempts = 0;
    
    @OneToOne(mappedBy="")
    private Kunde kunde;
    
    
    public Login(){}
    
    public Login(String email, String password){
        this.email = email;
        this.password = password;
    }
    
    @Override
    public boolean equals(Object obj){
        Login temp = (Login)obj;
        if (temp == null) return false;
        return (temp.email.equals(this.email) && temp.password.equals(this.password));
    }

    public long getUnlocktime() {
        return unlocktime;
    }

    public void setUnlocktime(long unlocktime) {
        this.unlocktime = unlocktime;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }
    
    
      
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }
    
    
}
