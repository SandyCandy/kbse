package de.hsos.kbse.pizza4me.entity;

import de.hsos.kbse.generic.EntityWithIntId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Bestellposten extends EntityWithIntId{   
    @OneToOne
    private Pizza pizza;
    
    private int anzahl = 0;

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
    
    public void increaseAnzahl(){
        anzahl++;
    }
    
    public void decreaseAnzahl(){
        anzahl--;
        if (anzahl < 0) anzahl = 0;
    }
    
    public double getPreis(){
        return pizza.getPreis() * anzahl;
    }
}
