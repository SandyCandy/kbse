package de.hsos.kbse.pizza4me.controller;

import de.hsos.kbse.generic.RepoForEntityWithIntId;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.pizza4me.entity.Pizza;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.TypedQuery;

@ApplicationScoped
public class BestellungRepo extends RepoForEntityWithIntId<Bestellung>{
    public BestellungRepo(){
        this.type = Bestellung.class;
    }
    
    @Produces
    public List<Bestellung> allBestellungs(){
        // Query
        TypedQuery<Bestellung> pizzaQuery = em.createQuery("select p from Bestellung p", Bestellung.class);
        return pizzaQuery.getResultList();
    }
}

