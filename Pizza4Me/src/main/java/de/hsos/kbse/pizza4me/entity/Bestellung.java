package de.hsos.kbse.pizza4me.entity;

import de.hsos.kbse.generic.EntityWithIntId;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Bestellung  extends EntityWithIntId{
        
    @ManyToOne      // Many orders To one customer
    Kunde kunde;
    
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Bestellposten> posten = new ArrayList<>();

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }

    public List<Bestellposten> getPosten() {
        return posten;
    }

    public void setPosten(List<Bestellposten> posten) {
        this.posten = posten;
    }
    
    public double getPrice() {
        double price = 0;
        price = this.getPosten().stream().map((tempPosten) -> tempPosten.getPreis()).reduce(price, (accumulator, _item) -> accumulator + _item);
        return price;
    }
    
    public String getNicePrice() {
        return String.format("%1$.2f€", getPrice());
    }
    
    
}
