package de.hsos.kbse.pizza4me.controller;

import de.hsos.kbse.generic.RepoForEntityWithIntId;
import de.hsos.kbse.pizza4me.entity.Adresse;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AdresseRepo extends RepoForEntityWithIntId<Adresse>{
    public AdresseRepo(){
        this.type = Adresse.class;
    }
}
