package de.hsos.kbse.pizza4me.boundary;

import de.hsos.kbse.pizza4me.controller.BestellungRepo;
import de.hsos.kbse.pizza4me.controller.KundeRepo;
import de.hsos.kbse.pizza4me.controller.LoginRepo;
import de.hsos.kbse.pizza4me.controller.PizzaRepo;
import de.hsos.kbse.pizza4me.entity.Bestellposten;
import de.hsos.kbse.pizza4me.entity.Bestellung;
import de.hsos.kbse.pizza4me.entity.Kunde;
import de.hsos.kbse.pizza4me.entity.Login;
import de.hsos.kbse.pizza4me.entity.Pizza;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

@Named("PizzaQuery")
@Stateless
public class PizzaQuery{
    @Inject
    private BestellungRepo bestellungen;
    @Inject
    private KundeRepo kunden;
    @Inject
    private LoginRepo login;
    @Inject
    private PizzaRepo pizzas;
   
    private Kunde kunde;
    
    private Bestellung bestellung;
    
    private String email;
    private String password;
    
    public PizzaQuery(){
        kunde = new Kunde();
        bestellung = new Bestellung();
    }
    
    public void init(){
        // Fill Database
        initDB();
    }
    
    public String authenticate(){
        // Get the login object from db
        Login temp = login.userLogin(email);
        if (temp == null){
            // This will happen if there was no login for this email
            System.out.println("Anmeldename exisitert nicht!");
            return "login_error.xhtml";
        }
        if (!temp.getPassword().equals(password)){
            // Wrong password
            // increase attempts
            temp.setAttempts(temp.getAttempts()+1);
            // Nur 3 Anmeldeversuche
            if (temp.getAttempts() >= 3){
                temp.setUnlocktime(System.currentTimeMillis()+10000);
                System.out.println("Account wurde gesperrt. Melden Sie sich im Restaurant!");
            }
            System.out.println("Fehlerhaftes Passwort!");
            return "login_error.xhtml";
        } else {
            if (temp.getUnlocktime() > System.currentTimeMillis()){
                System.out.println("Account gesperrt!");
                return "login_error.xhtml";
            }

            temp.setAttempts(0); // Anmeldeversuche
            System.out.println("Login erfolgreich.");
            
            // in DB persistieren
            bestellung.setKunde(temp.getKunde());            
            this.bestellungen.persist(bestellung);
            return "success.xhtml";
        }
    }

    public String start(){
        createOrder();
        return "order.xhtml";
    }
    
    public void createOrder(){
        this.bestellung = new Bestellung();
        
        List<Pizza> pizzaList = pizzas.allPizzas();
        for (Pizza tempPizza : pizzaList) {
            Bestellposten tempPosten = new Bestellposten();
            tempPosten.setPizza(tempPizza);
            this.bestellung.getPosten().add(tempPosten);
        }
    }
    
    public String submitOrder(){
        if (bestellung.getPrice() <= 0){
            return "order_error.xhtml";
        }
        return "login.xhtml";
    }
    
    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Bestellung getBestellung() {
        return bestellung;
    }

    public void setBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    private void initDB(){
        if (this.login.amount() > 0)
            return;
        Kunde tempKunde;
        
        tempKunde = new Kunde();
        tempKunde.setVorname("Jonas");
        tempKunde.setNachname("Uekoetter");
        tempKunde.setTelefon("01234/5678");
        
        tempKunde.getAdresse().setHausnummer("24");
        tempKunde.getAdresse().setOrt("Osnabrueck");
        tempKunde.getAdresse().setPostleitzahl("49078");
        tempKunde.getAdresse().setStrasse("Martinistrasse");
        
        tempKunde.getLogin().setEmail("jo@nas.com");
        tempKunde.getLogin().setPassword("pw");
        tempKunde.getLogin().setKunde(tempKunde);
        kunden.persist(tempKunde);
        
        tempKunde = new Kunde();
        tempKunde.setVorname("Sandra");
        tempKunde.setNachname("Tieben");
        tempKunde.setTelefon("2345/6789");
        tempKunde.getAdresse().setHausnummer("11");
        tempKunde.getAdresse().setOrt("Osnabrueck");
        tempKunde.getAdresse().setPostleitzahl("49076");
        tempKunde.getAdresse().setStrasse("Hasetorwall");
        tempKunde.getLogin().setEmail("san@dra.de");
        tempKunde.getLogin().setPassword("pw");
        tempKunde.getLogin().setKunde(tempKunde);
        kunden.persist(tempKunde);
        
        tempKunde = new Kunde();
        tempKunde.setVorname("Hans");
        tempKunde.setNachname("Wurst");
        tempKunde.setTelefon("444/5555");
        tempKunde.getAdresse().setHausnummer("66");
        tempKunde.getAdresse().setOrt("Meppen");
        tempKunde.getAdresse().setPostleitzahl("49733");
        tempKunde.getAdresse().setStrasse("Biergasse");
        tempKunde.getLogin().setEmail("hans@wurst.de");
        tempKunde.getLogin().setPassword("pw");
        tempKunde.getLogin().setKunde(tempKunde);
        kunden.persist(tempKunde);
        
        Pizza tempPizza;
        tempPizza = new Pizza();
        tempPizza.setName("Margherita");
        tempPizza.setBeschreibung("Einfach und lecker!");
        tempPizza.setPreis(5.55f);
        pizzas.persist(tempPizza);
        tempPizza = new Pizza();
        tempPizza.setName("Funghi");
        tempPizza.setBeschreibung("Frische Pilze aus dem Wald");
        tempPizza.setPreis(6.66f);
        pizzas.persist(tempPizza);
        tempPizza = new Pizza();
        tempPizza.setName("Salami");
        tempPizza.setBeschreibung("Ohne Pferd!");
        tempPizza.setPreis(7.77f);
        pizzas.persist(tempPizza);
        tempPizza = new Pizza();
        tempPizza.setName("Hawaii");
        tempPizza.setBeschreibung("Total international");
        tempPizza.setPreis(8.88f);
        pizzas.persist(tempPizza);
    }
}
