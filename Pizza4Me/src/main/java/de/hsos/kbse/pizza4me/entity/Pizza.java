
package de.hsos.kbse.pizza4me.entity;

import de.hsos.kbse.generic.EntityWithIntId;
import javax.persistence.Entity;

@Entity
public class Pizza  extends EntityWithIntId{
    private String name;
    private String beschreibung;
    private double preis;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public double getPreis() {
        return preis;
    }
    
    public String getNicePrice() {
        return String.format("%1$.2f€", getPreis());
    }

    public void setPreis(float preis) {
        this.preis = preis;
    }
    
    
}
