package de.hsos.kbse.pizza4me.controller;

import de.hsos.kbse.generic.RepoForEntityWithIntId;
import de.hsos.kbse.pizza4me.entity.Pizza;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.TypedQuery;

@ApplicationScoped
public class PizzaRepo extends RepoForEntityWithIntId<Pizza>{
    public PizzaRepo(){
        this.type = Pizza.class;
    }
    
    @Produces
    public List<Pizza> allPizzas(){
        // Query
        TypedQuery<Pizza> pizzaQuery = em.createQuery("select p from Pizza p", Pizza.class);
        return pizzaQuery.getResultList();
    }
}

