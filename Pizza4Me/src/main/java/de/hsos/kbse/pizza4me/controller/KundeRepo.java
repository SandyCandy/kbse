package de.hsos.kbse.pizza4me.controller;

import de.hsos.kbse.generic.RepoForEntityWithIntId;
import de.hsos.kbse.pizza4me.entity.Kunde;
import de.hsos.kbse.pizza4me.entity.Pizza;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.TypedQuery;

@RequestScoped
public class KundeRepo extends RepoForEntityWithIntId<Kunde>{
    public KundeRepo(){
        this.type = Kunde.class;
    }
    
    @Produces
    public List<Kunde> allKundes(){
        // Query
        TypedQuery<Kunde> pizzaQuery = em.createQuery("select p from Kunde p", Kunde.class);
        return pizzaQuery.getResultList();
    }
}

