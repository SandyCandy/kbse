package de.hsos.kbse.pizza4me.entity;

import de.hsos.kbse.generic.EntityWithIntId;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Kunde extends EntityWithIntId{

    private String vorname;
    private String nachname; 
    private String telefon;
    
    // 1:1 Relation, cascade => changes here are also saved to the login
    @OneToOne(cascade = CascadeType.PERSIST)
    private Login login;
    
    @OneToOne(cascade = CascadeType.PERSIST)
    private Adresse adresse;
    
    // 1 Customer => many Orders
    // Mapped by => identified by attribute "kunde" in "Bestellung"
    @OneToMany(mappedBy="kunde")
    private List<Bestellung> bestellungen = new ArrayList<>();
    
    public Kunde(){
        adresse = new Adresse();
        login = new Login();
    };

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<Bestellung> getBestellungen() {
        return bestellungen;
    }

    public void setBestellungen(List<Bestellung> bestellungen) {
        this.bestellungen = bestellungen;
    }
    
    
   
    
}
