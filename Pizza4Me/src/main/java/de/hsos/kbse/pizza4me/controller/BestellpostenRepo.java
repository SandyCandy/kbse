package de.hsos.kbse.pizza4me.controller;

import de.hsos.kbse.generic.RepoForEntityWithIntId;
import de.hsos.kbse.pizza4me.entity.Bestellposten;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class BestellpostenRepo extends RepoForEntityWithIntId<Bestellposten>{
    public BestellpostenRepo(){
        this.type = Bestellposten.class;
    }
}
