package de.hsos.kbse.logging.binding;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

// InterceptorBinding aehnlich Qualifier
// Allow an interceptor to be connected to this interface so you can use @LoggingInterface
// to use the respective interceptor
@Inherited
@InterceptorBinding
@Target({TYPE, METHOD})
@Retention(RUNTIME)
public @interface LoggingInterface {
    // Set this Parameter to Nonbinding so it isnt required when looking for an
    // appropriate interceptor
    @Nonbinding String logLevel() default "INFO";
}