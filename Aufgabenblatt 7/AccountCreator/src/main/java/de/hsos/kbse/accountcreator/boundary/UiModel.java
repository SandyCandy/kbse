package de.hsos.kbse.accountcreator.boundary;

import de.hsos.kbse.accountcreator.controller.UserRepository;
import de.hsos.kbse.accountcreator.entity.User;
import de.hsos.kbse.logging.binding.LoggingInterface;
import java.io.Serializable;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.flow.FlowScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("model")
@FlowScoped(value="accountFlow")
public class UiModel implements Serializable{
    private boolean optimize = false;
    private boolean display = false;
    
    private String password2;
    
    @Inject
    private Conversation conversation;

    
    @Inject
    private UserRepository allUser;
    
    @Inject
    private User user; //neuer Nutzer

    public boolean isOptimize() {
        //System.out.println(optimize);
        return optimize;
    }

    public void setOptimize(boolean optimize) {
        this.optimize = optimize;
    }
    
    public void toggleOptimize(){
        //System.out.println("Toggle");
        this.optimize = !this.optimize;
    }
    
    @LoggingInterface
    public String submitUserData(){
        System.out.println(user.toString());
        return "login";
    }
    
    @LoggingInterface
    public String submitLoginData(){
        if (getUser().getLogin().getPassword().equals(this.password2)){
            this.allUser.createUser(user);
            return "success";
        } else {
            return "fail";
        }
    }
    
    public void detailsCorrect(){
        this.display = true;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }
}
