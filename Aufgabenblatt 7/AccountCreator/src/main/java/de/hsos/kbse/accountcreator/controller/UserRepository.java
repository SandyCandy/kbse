package de.hsos.kbse.accountcreator.controller;

import de.hsos.kbse.accountcreator.entity.User;
import de.hsos.kbse.logging.binding.LoggingInterface;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository {
    private ConcurrentHashMap<String,User> userList = new ConcurrentHashMap<>();
    
    public User readUser(String id){
        return this.userList.get(id);
    }
    
    @LoggingInterface
    public boolean createUser(User user){
        if (user == null) return false;
        String id = user.getVorname()+user.getNachname();
        if (readUser(id) != null) return false;
        this.userList.put(id, user);
        System.out.println(user);
        return true;
    }
    
    public boolean updateUser(User user){
        if (user == null) return false;
        String id = user.getVorname()+user.getNachname();
        if (readUser(id) == null) return false;
        this.userList.replace(id, user);
        return true;
    }
    
    public boolean deleteUser(User user){
        if (user == null) return false;
        String id = user.getVorname()+user.getNachname();
        if (readUser(id) == null) return false;
        this.userList.remove(id);
        return true;
    }
}
