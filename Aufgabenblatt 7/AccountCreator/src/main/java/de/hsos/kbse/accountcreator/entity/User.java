package de.hsos.kbse.accountcreator.entity;

import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

@Dependent
public class User implements Serializable{
    private String Vorname = "";
    private String Nachname = "";
    
    @Inject
    private Profil profil;
    
    @Inject
    private Login login;

    @Override
    public String toString() {
        return "User{" + "Vorname=" + Vorname + ", Nachname=" + Nachname + ", profil=" + profil + ", login=" + login + '}';
    }
    
    
    
    public String getVorname() {
        return Vorname;
    }

    public void setVorname(String Vorname) {
        this.Vorname = Vorname;
    }

    public String getNachname() {
        return Nachname;
    }

    public void setNachname(String Nachname) {
        this.Nachname = Nachname;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }
    
    
}
