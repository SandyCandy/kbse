package de.hsos.kbse.accountcreator.entity;

import java.io.Serializable;
import javax.enterprise.context.Dependent;

@Dependent
public class Login implements Serializable{
    private String username = "";
    private String password = "";

    @Override
    public String toString() {
        return "Login{" + "username=" + username + ", password=" + password + '}';
    }
    
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
