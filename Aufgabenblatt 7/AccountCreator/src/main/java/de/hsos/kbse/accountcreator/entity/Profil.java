package de.hsos.kbse.accountcreator.entity;

import java.io.Serializable;
import javax.enterprise.context.Dependent;

@Dependent
public class Profil implements Serializable{
    private String Strasse = "";
    private String PLZ = "";
    private String Ort = "";
    private String Telefonnummer = "";

    @Override
    public String toString() {
        return "Profil{" + "Strasse=" + Strasse + ", PLZ=" + PLZ + ", Ort=" + Ort + ", Telefonnummer=" + Telefonnummer + '}';
    }

    
    
    public String getStrasse() {
        return Strasse;
    }

    public void setStrasse(String Strasse) {
        this.Strasse = Strasse;
    }

    public String getPLZ() {
        return PLZ;
    }

    public void setPLZ(String PLZ) {
        this.PLZ = PLZ;
    }

    public String getOrt() {
        return Ort;
    }

    public void setOrt(String Ort) {
        this.Ort = Ort;
    }

    public String getTelefonnummer() {
        return Telefonnummer;
    }

    public void setTelefonnummer(String Telefonnummer) {
        this.Telefonnummer = Telefonnummer;
    }
    
    
}
