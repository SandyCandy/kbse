package de.hsos.kbse.logging.interceptor;

import de.hsos.kbse.logging.binding.LoggingInterface;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@LoggingInterface   // This can be used for the LoggingInterface
@Interceptor
public class LogInterceptor implements Serializable{
    // Create a logger that can log things
    private Logger logger = Logger.getLogger(LogInterceptor.class.getName());
    
    // When an annotated method is invoked, this can be called
    @AroundInvoke
    public Object logMethodCall(InvocationContext ctx){
        // Get the parameters
        final LoggingInterface annotation = ctx.getMethod().getAnnotation(LoggingInterface.class);
        // now get and use the value from the parameter logLevel()
        Level lvl = getLoggerLevel(annotation.logLevel());

        // First log message
        logger.log(lvl, "[" + ctx.getTarget().getClass().getSimpleName() + "] " + ctx.getMethod().getName() + " has started!");
        // Result object that the function returns
        Object result = null;
        try{
            // Tell the function to continue
            result = ctx.proceed();
            // If we get here, the function has worked without an exception
            logger.log(lvl, "[" + ctx.getTarget().getClass().getSimpleName() + "] " + ctx.getMethod().getName() + " has ended successfully!");
        } catch (Exception e){
            // If we get an exception, we announce it
            logger.log(Level.SEVERE, "[" + ctx.getTarget().getClass().getSimpleName() + "] " + ctx.getMethod().getName() + " has failed!");
        }
        finally {
            // We always return the result
            return result;
        }
    }
    
    // Convert String to logger level
    private Level getLoggerLevel(String level){
        switch (level){
            case "INFO":
                return Level.INFO;
                //this.logger.setLevel(Level.INFO);
            case "SEVERE":
                //this.logger.setLevel(Level.SEVERE);
                return Level.SEVERE;
            case "WARNING":
                return Level.WARNING;
            case "FINE":
                return Level.FINE;
            default: 
                return Level.ALL;
        }
    }
}
