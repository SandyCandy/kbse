SessionScoped:
volle Funktionalität

RequestScoped: 
Daten werden nicht mehr gespeichert
Weiterleitung auf die nächste Seite funktioniert nicht mehr

ViewScoped:
Resetted direkt, z.B. auch beim Klicken auf die Checkbox und die Buttons

ConversationScoped:
Weiterleitung funktioniert
Daten im Profil werden nicht mehr gespeichert

FlowScoped:
volle Funktionalität



Beste Eignung: Flow-Scoped
Einzige voll funktionale Scope, der nicht die Nachteile von SessionScoped aufweist.
