/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.boundary;

import de.hsos.kbse.movie.impl.Movie;
import de.hsos.kbse.movie_if.GetMovies;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sandratieben
 */
@Named
@RequestScoped
public class Index implements Serializable {
    
    @Inject
    private GetMovies getMovies;

    @NotNull
    private String directorName;

    public Index() {
    }

    public GetMovies getGetMovies() {
      return getMovies;
    }

    public void setGetMovies(GetMovies getMovies) {
      this.getMovies = getMovies;
    }

    @NotNull @Size(min = 1)
    public String getDirectorName() {
      return directorName;
    }

    public void setDirectorName(String directorName) {
      this.directorName = directorName;
    }

    public void start() {
        //List<Movie> movies = new MovieLister().moviesDirectedBy(directorName);
        if(!isValid()) {
            FacesContext.getCurrentInstance().addMessage(null, 
                new FacesMessage("director field muss content haben"));
        }
        List<Movie> movies = getGetMovies().moviesDirectedBy(directorName);
        String moviesFormatted = "";
        moviesFormatted = movies.stream().map(m -> m.getTitle()).collect(Collectors.joining("\n"));
        FacesContext.getCurrentInstance().addMessage(null, 
                new FacesMessage(movies.size() + " movies by " + directorName + ":"));
        for(int i = 0; i < movies.size(); ++i) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(i+1 + ": " + movies.get(i).getTitle()));
        }
        
    }
    
    private boolean isValid() {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        // because we only validate one property we just want to know if there is an error or not
        // if there is an error the property is not valid
        return validator.validate(this).isEmpty();
    }
}
