/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.interceptor.impl;

import de.hsos.kbse.interceptor.Logged;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 *
 * @author sandratieben
 */

@Logged
@Interceptor
public class MethodLogInterceptor {
    private final static Logger logger = Logger.getLogger(MethodLogInterceptor.class.getName());
    
    @AroundInvoke
    public Object logMethodCall(InvocationContext ctx) throws Exception {
        Level logLevel = Level.INFO;
        
        try {
            String tmp = ctx.getMethod().getAnnotation(Logged.class).toString();
            String[] parts = tmp.split("=");
            tmp = parts[1].substring(0, parts[1].length() -1);
            logLevel = Level.parse(tmp);
        } catch(IllegalArgumentException t){}
        
        logger.log(logLevel, ctx.getMethod().getName() + " started ");
        Object result = ctx.proceed();
        logger.log(logLevel, ctx.getMethod().getName() + " ended");
        return result;
    }
}
