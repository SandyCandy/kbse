package de.hsos.kbse.movie.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;


@Named @ApplicationScoped
public class MovieBuilder {

    public MovieBuilder() {
    
    }
    
    public Movie newMovie(String directorName, String title) {
        return new Movie(directorName, title);
    }
}
