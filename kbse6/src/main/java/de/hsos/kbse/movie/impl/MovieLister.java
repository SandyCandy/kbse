/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movie.impl;

import de.hsos.kbse.interceptor.ClassLogging;
import de.hsos.kbse.interceptor.Logged;
import de.hsos.kbse.movie_if.GetMovies;
import de.hsos.kbse.qualifier.moviefinder.CSV;
import de.hsos.kbse.qualifier.moviefinder.DB;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named @ApplicationScoped
public class MovieLister implements GetMovies, Serializable
{
    //Field Injection
    @Inject @DB
    private List<Movie> movies;
    
    public MovieLister() {}

    @Logged(logLevel="WARNING")
    @Override
    public List<Movie> moviesDirectedBy(String directorName) {
        List <Movie> resultMovies = new ArrayList<>();
        if(movies == null) {  
            System.out.println("movies == null\nInjection failed!");
        }
        if(movies.size() == 0) {
            System.out.println("Error while loading movies!");
        }
        movies.stream()
                .filter(m -> m.getDirector()
                .contains(directorName))
                .forEach(m2 -> resultMovies.add(m2)); 
        return resultMovies;
    }

}
