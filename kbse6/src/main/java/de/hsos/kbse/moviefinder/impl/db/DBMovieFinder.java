/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.moviefinder.impl.db;

import de.hsos.kbse.movie.impl.Movie;
import de.hsos.kbse.movie.impl.MovieBuilder;
import de.hsos.kbse.moviefinder_if.MovieFinder;
import de.hsos.kbse.qualifier.moviefinder.DB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped 
@Named 
@DB
public class DBMovieFinder implements MovieFinder {
    
    private String conName;
    
   
    @Inject private MovieBuilder movieBuilder;

    /*Diese Methode liest eine Textdatei mit Filminformationen zeilenweise ein, erzeugt pro Zeile ein Objekt der Klasse
    Movie, verwaltet die erzeugten Objekte in einer Liste des Typs ArrayList<Movie> und gibt diese zurück*/
    public DBMovieFinder() {     
        conName = "jdbc:derby://localhost:1527/KBSE_2_JDBC";
    }

    public void setConnection(String connection) {
        this.conName = connection;
    }
       
    @Override
    @Produces @DB
    public List<Movie> findAllMovies() {
        List<Movie> movies = new ArrayList<>();
   
        try {
         
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            try (Connection con = DriverManager.getConnection(conName))
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM movies");
                while(rs.next())
                {
                    //System.out.println(rs.getInt(1) + " " + rs.getString(2) + ": " + rs.getString(3));
                    Movie m = new Movie();
                    m.setDirector(rs.getString(3));
                    m.setTitle(rs.getString(2));
                    
                    movies.add(m);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }   catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return movies;
    }
    
    @Override
    public List<Movie> findMoviesByDirector(String director) {
        
        if(director == null)
        {
            return null;
        }
        
        List<Movie> movies = new ArrayList<>();
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            try (Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/KBSE_2_JDBC"))
            {
                PreparedStatement pst = con.prepareStatement("Select * from movies where dname like ?");
                pst.setString(1, "%" + director +" %");
                ResultSet rs = pst.executeQuery();
                while(rs.next())
                {
                    Movie m = movieBuilder.newMovie(rs.getString(3), rs.getString(2));
                    movies.add(m);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            }

            return movies;
        }   catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(DBMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
            }

        return movies;
    }   
}
