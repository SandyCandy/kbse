package de.hsos.kbse.pizza.repository;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@MappedSuperclass   // No table created for this, information will be given to child classes
public abstract class EntityWithIntId implements Serializable{
    @Id @GeneratedValue(strategy = GenerationType.TABLE)
    protected Integer id;
    
    @Version
    private long version;
    
    public int getId(){
        return this.id.intValue();
    }
    
    public boolean equals(Object obj){
        if (obj == null || id ==null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        final EntityWithIntId other = (EntityWithIntId)obj;
        return (this.id == other.id);
    }
}
