/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.repository;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import de.hsos.kbse.pizza.entity.Bestellung;
import de.hsos.kbse.pizza.entity.Bestellposten;
import de.hsos.kbse.pizza.entity.Pizza;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@ApplicationScoped
//@Stateless
public class PizzaRepository extends RepoForEntityWithID {
    
    public PizzaRepository() {
        this.type = Bestellung.class;
    }
        
    @Produces
    //@PersistenceContext(unitName = "JPAPizzaPU")
    public List<Bestellposten> getPizzas(){
        List<Bestellposten> posten = new ArrayList<>();
        posten.add(new Bestellposten(1, new Pizza("Salami", "1 nices Teil vong Pizza her", 5.50)));
        posten.add(new Bestellposten(3, new Pizza("Funghi", "hallo, i bims, 1 funghipizza", 7.99)));
        posten.add(new Bestellposten(3, new Pizza("Mozzarella", "Lecker, lecker!", 7.20)));
        
        return posten;
    }
    
    public void removeBestellposten(Bestellposten posten) {
        // TODO
    }
    
    
}
