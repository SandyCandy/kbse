/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author sandratieben
 */
public class Bestellposten implements Serializable{
    private int menge;
    private Pizza pizza;

    public Bestellposten(){}
    
    public Bestellposten(int menge, Pizza p) {
        this.menge = menge;
        this.pizza = p;
    }
    
    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String toString() {
        return "Bestellposten:" + " Menge = " + menge + ", Typ = " + pizza + '.';
    }
    
    public double getPrice() {
        return pizza.getPrice() * menge;
    }
    
    public boolean isValidBestellposten(Bestellposten p) {
        boolean one = this.getPizza().getName().equals(p.getPizza().getName());
        
        boolean two = this.menge < p.menge;
        return one && two;
    }
}
