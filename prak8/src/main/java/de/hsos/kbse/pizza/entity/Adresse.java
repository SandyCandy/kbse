/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.entity;

import java.io.Serializable;
import javax.enterprise.context.Dependent;

@Dependent
public class Adresse implements Serializable {
    private String strasse = "";
    private String hausnr = "";
    private String plz = "";
    private String ort = "";

    @Override
    public String toString() {
        return "Adresse{" + "strasse=" + strasse + ", hausnr=" + hausnr + ", plz=" + plz + ", ort=" + ort + '}';
    }

    public String getStrasse() {
        return strasse;
    }

    public String getHausnr() {
        return hausnr;
    }

    public String getPlz() {
        return plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public void setHausnr(String hausnr) {
        this.hausnr = hausnr;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }
    
    
}
