/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.entity;

import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

@Dependent
public class Customer implements Serializable {
    private String vname;
    private String nname;
    private String telnr;
    
    @Inject
    private Adresse adresse;
    
    @Inject
    private Login login;
    
    @Inject
    private Bestellung bestellungen;
    
    public Customer(String un, String pw) {
        this.login = new Login(un, pw);
        bestellungen = new Bestellung();
    }

    @Override
    public String toString() {
        return "Kunde{" + "vname=" + vname + ", nname=" + nname + ", telnr=" + telnr + ", adresse=" + adresse + ", login=" + login + ", bestellungen=" + bestellungen + '}';
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getNname() {
        return nname;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public String getTelnr() {
        return telnr;
    }

    public void setTelnr(String telnr) {
        this.telnr = telnr;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Bestellung getBestellungen() {
        return bestellungen;
    }

    public void setBestellungen(Bestellung bestellungen) {
        this.bestellungen = bestellungen;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }
    
    
    
    
}
