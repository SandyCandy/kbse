/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.entity;

import java.io.Serializable;
import javax.enterprise.context.Dependent;

@Dependent
public class Login implements Serializable {
    private String email = "";
    private String pw = "";

    public Login() {
    }
    
    public Login(String un, String pw) {
        this.email = un;
        this.pw = pw;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    @Override
    public String toString() {
        return "Login{" + "email=" + email + ", pw=" + pw + '}';
    }
    
    
}
