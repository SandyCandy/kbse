/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.repository;

import de.hsos.kbse.pizza.entity.Customer;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CustomerRepository {
    private ConcurrentHashMap<String, Customer> customerList = new ConcurrentHashMap<>();

    public CustomerRepository() {
        customerList.put("sändycändy", new Customer("s@c.de", "passwort"));
        customerList.put("jonas", new Customer("s@c.de", "passwort"));
    }
    
    public boolean createCustomer(Customer customer) {
        if (customer == null) return false;
        String id = customer.getVname()+customer.getNname();
        if (readCustomer(id) != null) return false;
        this.customerList.put(id, customer);
        System.out.println(customer);
        return true;
    }
    
    public Customer readCustomer(String id) {
        return this.customerList.get(id);
    }
    
    public boolean updateCustomer(Customer customer) {
        if (customer == null) return false;
        String id = customer.getVname()+customer.getNname();
        if (readCustomer(id) != null) return false;
        this.customerList.put(id, customer);
        System.out.println(customer);
        return true;
    }
    
    public boolean deleteCustomer(Customer customer) {
        if (customer == null) return false;
        String id = customer.getVname()+customer.getNname();
        if (readCustomer(id) == null) return false;
        this.customerList.remove(id);
        return true;
    }
}
