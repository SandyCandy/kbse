/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.Dependent;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Dependent
public class Bestellung implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    private List<Bestellposten> bestellposten = new ArrayList<>();

    public List<Bestellposten> getBestellposten() {
        return bestellposten;
    }

    public void setBestellposten(List<Bestellposten> bestellposten) {
        this.bestellposten = bestellposten;
    }

    @Override
    public String toString() {
        return "Ihre Bestellung:" + "enthaelt" + bestellposten + '.';
    }
    
    public void addBestellposten(int amount, Pizza p) {
        addBestellposten(new Bestellposten(amount, p));
    }
    
    public void addBestellposten(Bestellposten b) {
        Optional<Bestellposten> o = bestellposten.stream()
                .filter(p -> p.getPizza().getName().equals(b.getPizza().getName()))
                .findFirst();
        if(o.isPresent()){
            o.get().setMenge(o.get().getMenge() + b.getMenge());
            return;
        }
        bestellposten.add(b);
    }
    
    public void removeBestellposten(Bestellposten p) {
        bestellposten.remove(p);
    }
    
    public double getPrice() {
        double price = 0;
        for(Bestellposten p : bestellposten) {
            price += p.getPrice();
        }
        return price;
    }
    
}
