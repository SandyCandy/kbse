/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.entity;

import java.io.Serializable;
import java.math.BigDecimal;


public class Pizza implements Serializable {
    private String name = "";
    private String beschreibung = "";
    private double price;

    public Pizza() {
        name = "Margaritha";
        beschreibung = "Der Klassiker!";
        price = 5;
    }
    
    public Pizza(String name, String desc, double price) {
        this.name = name;
        this.beschreibung = desc;
        this.price = price;
    }    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    @Override
    public String toString() {
        return "Pizza " + name + "(" + beschreibung + "): " + price + "€";
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
}
