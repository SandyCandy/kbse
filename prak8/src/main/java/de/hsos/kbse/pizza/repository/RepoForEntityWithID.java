/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.pizza.repository;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class RepoForEntityWithID<T extends EntityWithIntId> implements Serializable {
    @PersistenceContext(unitName="JPAPizzaPU")
    protected EntityManager em;
    protected Class<?> type;
    
    public void persist(T entity) {
        em.persist(entity);
    }
    
    public T findById(int id) {
        return (T) em.find(this.type, id);
    }
    
    public void remove(T entity) {
        em.remove(entity);
    }
    
    public T merge(T entity) {
        return em.merge(entity);
    }
}
