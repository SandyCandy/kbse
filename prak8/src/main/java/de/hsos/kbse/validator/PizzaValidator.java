/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.validator;

import de.hsos.kbse.pizza.entity.Bestellposten;
import de.hsos.kbse.pizza.entity.Bestellung;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;

/**
 *
 * @author jonas
 */
public class PizzaValidator {
    
    public enum PizzaPieceEnterpriseNameErrorEnum {
        keinePizzasMehr,
        pizzaUnwuerdig,
        sehrPizzaWuerdig,
        sonstigeFehlerDieDerPizzaImWegStehen
    }
    
    @Inject
    private List<Bestellposten> posten;
    
    // TODO OOOOOOO
    public List<String> validateBestellung(Bestellung b) {
        List<String> error = new ArrayList<>();
        for(Bestellposten p : posten) {
            for(Bestellposten pp : b.getBestellposten()) {
                if(p.getPizza().getName().equals(pp.getPizza().getName())) {
                    if (p.getMenge() < pp.getMenge()){
                        error.add("Du hast zuviel " + p.getPizza().getName() + " im Warenkorb");
                    }
                }
            }
        }
        return error;
    }
    
}
