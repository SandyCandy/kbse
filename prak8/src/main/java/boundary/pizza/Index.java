/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.pizza;

import de.hsos.kbse.pizza.entity.Bestellung;
import de.hsos.kbse.pizza.entity.Bestellposten;
import de.hsos.kbse.pizza.entity.Customer;
import de.hsos.kbse.pizza.repository.PizzaRepository;
import de.hsos.kbse.pizza.repository.CustomerRepository;
import de.hsos.kbse.validator.PizzaValidator;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

//@ApplicationScoped
@Named("index")
@Stateless
public class Index {
    
    @Inject 
    private PizzaRepository pizzaRepo;
    
    @Inject
    private CustomerRepository customerRepo;
    
    @Inject
    private List<Bestellposten> posten;
    
    @Inject
    private PizzaValidator validator;
    
    private Customer customer;
    //private Bestellung bestellung;
    
    private String username;
    private String pw;
    
    private String errorMessage;
    
    private int fails = 0;
   
    public List<Bestellposten> getPosten() {
        return posten;
    }

    public void setPosten(List<Bestellposten> posten) {
        this.posten = posten;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getFails() {
        return fails;
    }

    public void setFails(int fails) {
        this.fails = fails;
    }
    
    public String login() {
        Customer c = customerRepo.readCustomer(username);
        if (c == null || !c.getLogin().getPw().equals(pw) || fails > 3) {
            fails++;
            // kunde wird bis zum nächsten ablauf seines scopes gesperrt
            // zeit ist eine illusion, nur scopes sind real :woah"
            errorMessage += " du hast dich ausgesperrt :(";
            return "fail";
        }
        this.customer = c;
        return "bestellen";
    }
    
    public String buy() {
        List<String> error = validator.validateBestellung(customer.getBestellungen());
        if(error.isEmpty()) {
            return "success";
        }
        errorMessage = error.stream().collect(Collectors.joining("<br>"));
        return "fail";
    }
    
    private void initDB(){
    //TODO
    }
    
}
