/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.interceptor.impl;

import de.hsos.kbse.interceptor.ClassLogging;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@ClassLogging
@Interceptor
public class ClassLogInterceptor {
    private final static Logger logger = Logger.getLogger(ClassLogInterceptor.class.getName());
    
    @AroundInvoke
    public Object logClassCall(InvocationContext ctx) {
        Level logLevel = Level.INFO;
        
        try {
            // TODO: richtige Klasse
            if (ctx.getClass().getAnnotation(ClassLogging.class) != null) {
                String tmp = ctx.getClass().getAnnotation(ClassLogging.class).toString();
                System.out.println(tmp);
                String[] parts = tmp.split("=");
                tmp = parts[1].substring(0, parts[1].length() -1);
                logLevel = Level.parse(tmp);
            }
        } catch(IllegalArgumentException t){}
        
        logger.log(logLevel, "ClassLogInterceptor - started ");
        Object result = null;
        try {
            result = ctx.proceed();
        } catch (Exception ex) {
            Logger.getLogger(ClassLogInterceptor.class.getName()).log(Level.SEVERE, null, ex);
        }
        logger.log(logLevel, "ClassLogInterceptor - ended");
        return result;
    }
}
