/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movie.impl;

import de.hsos.kbse.movie_if.GetMovies;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

/**
 *
 * @author sandratieben
 */

@Decorator
public class MovieListerSorted implements GetMovies, Serializable {
    @Delegate //Delegation-Pattern; markiert Injektionspunkt
    @Inject @Any //ansonsten @Default
    private GetMovies movieData;

    @Override
    public List<Movie> moviesDirectedBy(String directorName) {
        List<Movie> sortTheList = movieData.moviesDirectedBy(directorName);
        sortTheList.sort((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
        
        return sortTheList;
    }   
}
