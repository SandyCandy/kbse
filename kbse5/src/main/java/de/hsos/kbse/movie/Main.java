package de.hsos.kbse.movie;

import de.hsos.kbse.interceptor.Logged;
import de.hsos.kbse.movie.impl.MovieLister;
import de.hsos.kbse.movie.impl.Movie;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.jboss.weld.environment.se.Weld;

/**
 *
 * @author SandyCandy
 */
public class Main implements Serializable {
    
    //private static Logger theLogger = Logger.getLogger(Main.class.getName());
        
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String yourFilePath = "logging.properties";
        try {
            LogManager.getLogManager().readConfiguration(new FileInputStream(yourFilePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Find the movies!");
        
        String director;
  
        MovieLister getMovies = new Weld().initialize().instance().select( MovieLister.class ).get();
        
        
        while(true)
        {
            String newSearch;
            System.out.println("Enter a Name:");
            
            director = scanner.nextLine();
            
            List<Movie> myMovies = getMovies.moviesDirectedBy(director);
            System.out.println("Searching for movies by " + director + "...");

            System.out.println("\n" + myMovies.size() + " movies found!");

            for(Movie t : myMovies)
            {
                System.out.println(t.getTitle());
            }
            
            System.out.println("\nNew search? (Press 'y')\nStop searching? (Press any other key)\n");

            newSearch = scanner.nextLine();
            if(!newSearch.equalsIgnoreCase("y"))
            {
                break;
            }
            
        }
        
        System.out.println("Bye, bye!");
        

    }
    
}
