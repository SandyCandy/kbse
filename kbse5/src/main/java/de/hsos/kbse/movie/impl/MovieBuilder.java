package de.hsos.kbse.movie.impl;

import javax.inject.Named;
import javax.inject.Singleton;


@Singleton
@Named
public class MovieBuilder {

    public MovieBuilder() {
    
    }
    
    public Movie newMovie(String directorName, String title) {
        return new Movie(directorName, title);
    }
}
