/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.moviefinder_if;

import de.hsos.kbse.movie.impl.Movie;
import java.util.List;


public interface MovieFinder {
    public List<Movie> findAllMovies();
    public List<Movie> findMoviesByDirector(String director);
}
