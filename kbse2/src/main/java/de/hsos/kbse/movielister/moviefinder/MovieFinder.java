/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.moviefinder;

import de.hsos.kbse.movielister.movie.Movie;
import java.util.ArrayList;

/**
 *
 * @author SandyCandy
 */
public interface MovieFinder {
    public ArrayList<Movie> findAllMovies();
    public ArrayList<Movie> findMoviesByDirector(String director);
}
