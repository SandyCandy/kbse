/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.moviefinder;

import de.hsos.kbse.movielister.movie.Movie;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SandyCandy
 */
public class ColonDelimitedMovieFinder implements MovieFinder {
    private String filename; //speichert den Pfad zur Textdatei

    /*Diese Methode liest eine Textdatei mit Filminformationen zeilenweise ein, erzeugt pro Zeile ein Objekt der Klasse
    Movie, verwaltet die erzeugten Objekte in einer Liste des Typs ArrayList<Movie> und gibt diese zurück*/
    public ColonDelimitedMovieFinder() {
        filename = "movies.txt";
    }

    public ColonDelimitedMovieFinder(String filename) {
        this.filename = filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    /**
     *
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Override
    public ArrayList<Movie> findAllMovies() {
        ArrayList<Movie> movies = new ArrayList<>();
        
        try {
            FileReader fileReader;
            String line;
            
            fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while ((line = bufferedReader.readLine()) != null) {
                movies.add(colomDelimitedStringToMovie(line));
            }
            bufferedReader.close();
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ColonDelimitedMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ColonDelimitedMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return movies;
    }
    
    private Movie colomDelimitedStringToMovie (String str) {
        String[] parts = str.split(":");
        
        Movie m = new Movie();
        
        m.setDirector(parts[1].trim());
        m.setTitle(parts[0].trim());
        
        return m;
    }
    
    /**
     *
     * @param director
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Override
        public ArrayList<Movie> findMoviesByDirector(String director) {
        FileReader fileReader;
        ArrayList<Movie> movies = new ArrayList<>();
        String line;

        try {        
            fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split(":");

                if(parts[1].contains(director))
                {
                    movies.add(colomDelimitedStringToMovie(line));
                }

            }

            bufferedReader.close();
        
        } catch (Exception ex) {
            Logger.getLogger(ColonDelimitedMovieFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return movies;
    }
}
