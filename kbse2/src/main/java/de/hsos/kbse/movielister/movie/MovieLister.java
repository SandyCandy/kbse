/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.movie;

import de.hsos.kbse.movielister.movie.Movie;
import de.hsos.kbse.movielister.moviefinder.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SandyCandy
 */
public class MovieLister implements GetMovies
{
    MovieFinder movieFinder;
    
    public MovieLister(MovieFinder mf) {
        this.movieFinder = mf;
    }

    public void setMovieFinder(MovieFinder movieFinder) {
        this.movieFinder = movieFinder;
    }

    @Override
    public Movie[] moviesDirectedBy(String directorName) {
        Movie[] resultMovies;
        //Better use: movieFinder.findMoviesByDirector(directorname)
        return resultMovies = (Movie[]) this.movieFinder.findAllMovies()
                .stream()
                .filter(m -> m.getDirector().contains(directorName))
                .toArray(Movie[]::new);        
    }
    
}
