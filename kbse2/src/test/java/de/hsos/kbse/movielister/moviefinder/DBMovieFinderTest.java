/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.moviefinder;

import de.hsos.kbse.movielister.movie.Movie;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SandyCandy
 */
public class DBMovieFinderTest {
    
    DBMovieFinder dbmf;
    ArrayList<Movie> m;
    
    public DBMovieFinderTest() {       
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        dbmf = new DBMovieFinder();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void findAllMovies() {
        m = dbmf.findAllMovies();
        assertTrue(m.size() > 0);
    }
    
    @Test
    public void findMoviesByDirector() {
        m = dbmf.findMoviesByDirector("Georg Lukas");
        assertTrue(m.size() == 0);
        
        m = dbmf.findMoviesByDirector("");
        ArrayList<Movie> all = dbmf.findAllMovies();
        assertTrue(m.size() == all.size());
    }
    
}
