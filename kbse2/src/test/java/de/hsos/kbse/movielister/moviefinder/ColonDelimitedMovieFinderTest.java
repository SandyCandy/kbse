/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.moviefinder;

import de.hsos.kbse.movielister.movie.Movie;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SandyCandy
 */
public class ColonDelimitedMovieFinderTest {
    
    ColonDelimitedMovieFinder dbmf;
    ArrayList<Movie> m;
    
    public ColonDelimitedMovieFinderTest() {
    }
    
    @Before
    public void setUp() {
        dbmf = new ColonDelimitedMovieFinder();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void findAllMovies() {
        m = dbmf.findAllMovies();
        assertTrue(m.size() > 0);
    }
    
    @Test
    public void findMoviesByDirector() {
        m = dbmf.findMoviesByDirector("George Lucas");
        assertTrue(m.size() == 1);
        
        m = dbmf.findMoviesByDirector("Georg Lukas");
        assertTrue(m.size() == 0);
        
        m = dbmf.findMoviesByDirector("");
        ArrayList<Movie> all = dbmf.findAllMovies();
        assertTrue(m.size() == all.size());
    }
}
