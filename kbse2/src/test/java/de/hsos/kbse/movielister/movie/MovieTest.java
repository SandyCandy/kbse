/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.movie;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SandyCandy
 */
public class MovieTest {
    
    Movie test;
    
    public MovieTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.test = new Movie();
        this.test.setDirector("David Lynch");
        this.test.setTitle("Fight Club");
    }
    
    @After
    public void tearDown() {
        test = null;
    }

    @Test
    public void getDirector() {
        String director = this.test.getDirector();
        assertTrue("David Lynch".equals(director));
    }
    
    @Test
    public void getTitle() {
        String title = this.test.getTitle();
        assertTrue("Fight Club".equals(title));
    }
}
