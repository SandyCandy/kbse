/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsos.kbse.movielister.movie;

import de.hsos.kbse.movielister.moviefinder.ColonDelimitedMovieFinder;
import de.hsos.kbse.movielister.moviefinder.DBMovieFinder;
import junit.framework.TestCase;
import static junit.framework.TestCase.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author SandyCandy
 */
public class MovieListerTest {
    
    Movie testMovie;
    
    @Before
    public void setUp() {
        this.testMovie = new Movie();
        this.testMovie.setDirector("George Lucas");
        this.testMovie.setTitle("Star Wars");
    }
    
    @After
    public void tearDown() {
        testMovie = null;
    }
    
    @Test
    public void testDBMovieFinder() {
       MovieLister mv = new MovieLister(new DBMovieFinder());
       Movie[] movies = mv.moviesDirectedBy("George Lucas");
       
       assertTrue(movies.length == 1);
       
       assertTrue(testMovie.equals(movies[0]));
       
       movies = mv.moviesDirectedBy("fdjfdkjkasdfjkafdsjdsajklsdfölkf");
       assertTrue(movies.length == 0);
    }
    
    @Test
    public void testColonDelimitedMovieFinder() {
       MovieLister mv = new MovieLister(new ColonDelimitedMovieFinder());
       Movie[] movies = mv.moviesDirectedBy("George Lucas");
       
       assertTrue(movies.length == 1);
       assertTrue(testMovie.equals(movies[0]));
       
       movies = mv.moviesDirectedBy("fdjfdkjkasdfjkafdsjdsajklsdfölkf");
       assertTrue(movies.length == 0);
    }
    

}
