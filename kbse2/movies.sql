Insert into movies values (1, 'Star Wars', 'George Lucas');
Insert into movies values (2, 'Pulp Fiction', 'Quentin Tarantino');
Insert into movies values (3, 'Der Soldat James Ryan', 'Steven Spielberg');
Insert into movies values (4, 'Trainspotting', 'Danny Boyle');
Insert into movies values (5, 'Jurassic Park', 'Steven Spielberg');
Insert into movies values (7, 'Einer flog über das Kuckucksnest', 'Milos Forman');
Insert into movies values (8, 'Das Schweigen der Lämmer', 'Jonathan Demme');
Insert into movies values (9, 'Minority Report', 'Steven Spielberg');
